# React Template
A template for React have best setting
`React`, `TypeScript`, `Vite`, `ESlint`, `GitLab Page`

## Project setup
```
$ yarn install
```
## Compiles and hot-reloads for development
```
$ yarn dev
```
## Compiles and minifies for production
```
$ yarn build
```
## Preview build file
```bash
$ yarn preview
```
## Lints
```
$ yarn lint
```

